class ArrowButton extends Button {
    constructor(grid, x, y, cx, cy, width, height, direction=0) {
        super(grid, x, y, cx, cy, width, height);
        
        this.type = BTN_ARROW;
        this.direction = direction;

        const shapeSize = width * 0.625;
        this.shape = new CustomShape(CustomShapes.one, this.x, this.y, 0, shapeSize);
        
        switch (this.direction) {
            // 왼쪽 방향
            case DIR_LEFT:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, 180, shapeSize);
                break;

            // 오른쪽 방향
            case DIR_RIGHT:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, 0, shapeSize);
                break;

            // 위쪽 방향
            case DIR_UP:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, -90, shapeSize);
                break;

            // 아래쪽 방향
            case DIR_DOWN:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, 90, shapeSize);
                break;

            // 좌우 방향
            case DIR_LR:
                this.shape = new CustomShape(CustomShapes.both, this.x, this.y, 0, shapeSize);
                break;

            // 상하 방향
            case DIR_UD:
                this.shape = new CustomShape(CustomShapes.both, this.x, this.y, 90, shapeSize);
                break;

            // 좌우상하 방향
            case DIR_LRUD:
                this.shape = new CustomShape(CustomShapes.four, this.x, this.y, 0, shapeSize);
                break;
            // 대각선 왼쪽-위
            case DIR_DLU:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, 225, shapeSize);
                break;
            // 대각선 왼쪽-아래
            case DIR_DLD:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, 135, shapeSize);
                break;
            // 대각선 오른쪽-위
            case DIR_DRU:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, -45, shapeSize);
                break;
            // 대각선 오른쪽-아래
            case DIR_DRD:
                this.shape = new CustomShape(CustomShapes.one, this.x, this.y, 45, shapeSize);
                break;
            // 대각선 왼쪽-위아래
            case DIR_DLUD:
                this.shape = new CustomShape(CustomShapes.both, this.x, this.y, 45, shapeSize);
                break;
            // 대각선 오른쪽-위아래
            case DIR_DRUD:
                this.shape = new CustomShape(CustomShapes.both, this.x, this.y, -45, shapeSize);
                break;
        }
    }

    onRealClicked() {
        super.onRealClicked();
        
        switch (this.direction) {
            // 왼쪽 방향
            case DIR_LEFT:
                for (let x = 0; x < this.cx; x++) {
                    if (grid.checkRange(x, this.cy)) {
                        grid.map[x][this.cy].toggle(false);
                    }
                }
                break;

            // 오른쪽 방향
            case DIR_RIGHT:
                for (let x = this.cx + 1; x < grid.numCols; x++) {
                    if (grid.checkRange(x, this.cy)) {
                        grid.map[x][this.cy].toggle(false);
                    }
                }
                break;

            // 위쪽 방향
            case DIR_UP:
                for (let y = this.cy + 1; y < grid.numRows; y++) {
                    if (grid.checkRange(this.cx, y)) {
                        grid.map[this.cx][y].toggle(false);
                    }
                }
                break;

            // 아래쪽 방향
            case DIR_DOWN:
                for (let y = 0; y < this.cy; y++) {
                    if (grid.checkRange(this.cx, y)) {
                        grid.map[this.cx][y].toggle(false);
                    }
                }
                break;

            // 좌우 방향
            case DIR_LR:
                for (let x = 0; x < grid.numCols; x++) {
                    if (grid.checkRange(x, this.cy) && x != this.cx) {
                        grid.map[x][this.cy].toggle(false);
                    }
                }
                break;

            // 상하 방향
            case DIR_UD:
                for (let y = 0; y < grid.numRows; y++) {
                    if (grid.checkRange(this.cx, y) && y != this.cy) {
                        grid.map[this.cx][y].toggle(false);
                    }
                }
                break;

            // 좌우상하 방향
            case DIR_LRUD:
                for (let x = 0; x < grid.numCols; x++) {
                    if (grid.checkRange(x, this.cy) && x != this.cx) {
                        grid.map[x][this.cy].toggle(false);
                    }
                }
                for (let y = 0; y < grid.numRows; y++) {
                    if (grid.checkRange(this.cx, y) && y != this.cy) {
                        grid.map[this.cx][y].toggle(false);
                    }
                }
                break;
            // 대각선 왼쪽-위
            case DIR_DLU:
                this.toggleDiagonal(-1, 1);
                break;
            // 대각선 왼쪽-아래
            case DIR_DLD:
                this.toggleDiagonal(-1, -1);
                break;
            // 대각선 오른쪽-위
            case DIR_DRU:
                this.toggleDiagonal(1, 1);
                break;
            // 대각선 오른쪽-아래
            case DIR_DRD:
                this.toggleDiagonal(1, -1);
                break;
            // 대각선 왼쪽-위아래
            case DIR_DLUD:
                this.toggleDiagonalBoth(-1);
                break;
            // 대각선 오른쪽-위아래
            case DIR_DRUD:
                this.toggleDiagonalBoth(1);
                break;
        }
    }

    toggleDiagonal(sx, sy) {
        for (let k = 1; k < grid.numRows; k++) {
            let x = this.cx + sx * k, y = this.cy + sy * k;
            if (!grid.checkRange(x, y)) {
                break;
            }
            grid.map[x][y].toggle(false);
        }
    }

    toggleDiagonalBoth(sx) {
        for (let k = -grid.numRows; k < grid.numRows; k++) {
            let x = this.cx + sx * k, y = this.cy + k;
            if (grid.checkRange(x, y) && k != 0) {
                grid.map[x][y].toggle(false);
            }
        }
    }

    draw() {
        super.draw();
        noStroke();
        fill(this.state ? Colors.HIGHLIGHT_BORDER_DEFAULT : Colors.NORMAL_BORDER_DEFAULT);

        this.shape.x = this.x;
        this.shape.y = this.y;
        this.shape.draw();
        //drawShape(this.x, this.y, 135, vertices2, 50);
    }
}