/*
 * Game (p5.js)
 */

var grid;

let onCount = 0;
function setup() {
    initGlobals();
    createCanvas(windowWidth, windowHeight);
    
    let width = getRandomInt(2, 10);
    let height = getRandomInt(2, 10);
    grid = new Grid(width, height, 50, 1.5);
    
    for (let col = 0; col < width; col++) {
        for (let row = 0; row < height; row++) {
            if (Math.random() < 0.8) {
                grid.putArrowButton(col, row, getRandomInt(0, 13));
            } else {
                grid.putNormalButton(col, row, false);
            }
        }
    }

    for (let i = 0; i < 22; i++) {
        let isArrow = false;

        let gridX = 0, gridY = 0;
        while (!isArrow) {
            gridX = getRandomInt(0, width);
            gridY = getRandomInt(0, height);

            if (grid.get(gridX, gridY).type == BTN_ARROW) {
                break;
            }
        }

        grid.toggle(gridX, gridY);
    }

    onCount = grid.getOnCount();
}

function mouseClicked() {
    grid.onMouseClicked();
    onCount = grid.getOnCount();
}

function mouseReleased() {
    grid.onMouseReleased();
    onCount = grid.getOnCount();
}
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
    grid.updateLayout();
}

function getCenter(vertices) {
    let count = vertices.length;

    let sumX = 0, sumY = 0;
    for (let i = 0; i < count; i++) {
        let v = vertices[i];
        sumX += v.x;
        sumY += v.y;
    }

    return [sumX / count, sumY / count];
}

function drawShape(x, y, degrees, vertices, scale) {
    angleMode(DEGREES);
    let count = vertices.length;
    const [centerX, centerY] = getCenter(vertices);

    beginShape();
    for (let i = 0; i < count; i++) {
        let v = vertices[i];

        let oldX = v.x - centerX, oldY = v.y - centerY;

        let newX = oldX * cos(degrees) - oldY * sin(degrees);
        let newY = oldX * sin(degrees) + oldY * cos(degrees);

        vertex(x + newX * scale, y + newY * scale);
    }
    endShape(CLOSE);
}

function draw() {
    clear();


    let textY = windowHeight * 0.08;

   
    grid.draw();
    //angle += 3;

    stroke(255);
    strokeWeight(5);
    fill(0);
    textSize(48);
    textAlign(CENTER);
    text(onCount, windowWidth / 2, textY);

    textSize(24);
    text("(Goal: 0)", windowWidth / 2, textY + 20);
}