class Colors {}

// 버튼 종류
const BTN_NORMAL = 0;
const BTN_ARROW = 1;

// 한 개의 직선 방향 (L, R, U, D)
const DIR_LEFT = 0;
const DIR_RIGHT = 1;
const DIR_UP = 2;
const DIR_DOWN = 3;

// 두 개의 직선 방향 (LR, UD)
const DIR_LR = 4;
const DIR_UD = 5;

// 모든 직선방향 (LRUD)
const DIR_LRUD = 6;

// 대각선 왼쪽 상/하방향
const DIR_DLU = 7;
const DIR_DLD = 8;

// 대각선 오른쪽 상/하방향
const DIR_DRU = 9;
const DIR_DRD = 10;

// 대각선 왼쪽/오른쪽 상하방향
const DIR_DLUD = 11;
const DIR_DRUD = 12;


function initGlobals() {
    
    // Normal Button
    Colors.NORMAL_BORDER_DEFAULT = color(180, 180, 180);
    Colors.NORMAL_FILL_DEFAULT = color(220, 220, 220);
    Colors.NORMAL_FILL_HOVER = color(240, 240, 240);
    Colors.NORMAL_FILL_PRESS = color(190, 190, 190);

    // Highlight Button
    Colors.HIGHLIGHT_BORDER_DEFAULT = color(179, 143, 0);
    Colors.HIGHLIGHT_FILL_DEFAULT = color(255, 204, 0);
    Colors.HIGHLIGHT_FILL_HOVER = color(255, 230, 128);
    Colors.HIGHLIGHT_FILL_PRESS = color(230, 184, 0);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }