/*
 * Button Class
 */
class Button {
    constructor(grid, x, y, cx, cy, width, height) {
        this.grid = grid;
        this.borderWeight = this.grid.borderWeight;

        this.x = x;
        this.y = y;
        this.cx = cx;
        this.cy = cy;
        this.width = width;
        this.height = height;

        this.canToggle = true;
        this.canToggleByOther = true;

        this.state = false;
        this.id = ++Button.id; 

        this.type = BTN_NORMAL;
    }
    isHover() {
        let startX = this.x - (this.width * 0.5), startY = this.y - (this.height * 0.5);
        let endX = startX + this.width, endY = startY + this.height;
        if (mouseX >= startX + 1 && mouseX <= endX - 1) {
            if (mouseY >= startY + 1 && mouseY <= endY - 1) {
                return true;
            }
        }
        return false;
    }
    onClicked() {
        if (!this.isHover() || this.id != Button.pressId) {
            return;
        }
        this.onRealClicked();
        this.toggle(true);
    }

    onRealClicked() {
        Button.pressId = -1;
    }

    onMacro() {
        this.onRealClicked();
        this.toggle(true);
    }

    onReleased() {
        if (Button.pressId == this.id && !this.isHover()) {
            Button.pressId = -1;
        }
    }

    toggle(isSelf) {
        if ((isSelf && this.canToggle) || (!isSelf && this.canToggleByOther)) {
            this.state = !this.state;
        }
    }

    draw() {
        if (this.canToggle && this.isHover() && (Button.pressId == -1 || Button.pressId == this.id)) {
            if (mouseIsPressed && mouseButton == LEFT) {
                fill(this.state ? Colors.HIGHLIGHT_FILL_PRESS : Colors.NORMAL_FILL_PRESS);
                Button.pressId = this.id;
            } else {
                fill(this.state ? Colors.HIGHLIGHT_FILL_HOVER : Colors.NORMAL_FILL_HOVER);
            }
        } else {
            fill(this.state ? Colors.HIGHLIGHT_FILL_DEFAULT : Colors.NORMAL_FILL_DEFAULT);
        }
        

        strokeWeight(this.borderWeight);
        stroke(this.state ? Colors.HIGHLIGHT_BORDER_DEFAULT : Colors.NORMAL_BORDER_DEFAULT);
        rect(this.x - (this.width * 0.5), this.y - (this.height * 0.5), this.width, this.height);
    }
}
Button.id = -1;
Button.pressId = -1;

