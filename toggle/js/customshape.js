// 화살표 모양 노가다
var CustomShapes = {
  one : [
    new Vec2(0, 0.45),
    new Vec2(0.5, 0.45),
    new Vec2(0.5, 0.7),
    new Vec2(0.75, 0.375),
    new Vec2(0.5, 0.05),
    new Vec2(0.5, 0.3),
    new Vec2(0, 0.3)
],
  both : [
    new Vec2(0.25, 0.05),
    new Vec2(0, 0.375),
    new Vec2(0.25, 0.7),
    new Vec2(0.25, 0.45),
    new Vec2(0.5, 0.45),
    new Vec2(0.5, 0.7),
    new Vec2(0.75, 0.375),
    new Vec2(0.5, 0.05),
    new Vec2(0.5, 0.3),
    new Vec2(0.25, 0.3)
  ],
  four : [
    new Vec2(0, 0.25),
    new Vec2(-0.25, 0),
    new Vec2(0, -0.25),
    new Vec2(0.25, 0)
  ]
};

class CustomShape {
    constructor(vertices, x, y, degrees, scale) {
        this.vertices = new Array();
        this.drawVertices = new Array();

        for (let i = 0; i < vertices.length; i++) {
            this.vertices.push(vertices[i].clone());
            this.drawVertices.push(vertices[i].clone());
        }
        this.x = x;
        this.y = y;
        this.degrees = degrees;
        this.scale = scale;

        
        this.calculate();
    }
    
    getCenter() {
        let count = this.vertices.length;

        let sumX = 0, sumY = 0;
        for (let i = 0; i < count; i++) {
            let v = this.vertices[i];
            sumX += v.x;
            sumY += v.y;
        }

        return new Vec2(sumX / count, sumY / count);
    }
    calculate() {
        let arrayLength = this.vertices.length;
        let centerPos = this.getCenter();

        angleMode(DEGREES);
        for (let i = 0; i < arrayLength; i++) {
            let srcVec = this.vertices[i].clone();
            let destVec = this.drawVertices[i];

            srcVec.x -= centerPos.x;
            srcVec.y -= centerPos.y;

            let transX = srcVec.x * cos(this.degrees) - srcVec.y * sin(this.degrees);
            let transY = srcVec.x * sin(this.degrees) + srcVec.y * cos(this.degrees);

            destVec.x = transX * this.scale;
            destVec.y = transY * this.scale;
        }
    }
    draw() {
        beginShape();

        let count = this.drawVertices.length;
        for (let i = 0; i < count; i++) {
            let v = this.drawVertices[i];
            vertex(this.x + v.x, this.y + v.y);
        }
        endShape(CLOSE);
    }
}