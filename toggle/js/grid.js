/*
 * Grid Class
 */
class Grid {
    constructor(cols, rows, btnSize, borderWeight) {
        this.numCols = cols;
        this.numRows = rows;
        this.buttonSize = btnSize;
        this.map = new Array(this.numCols);
        this.borderWeight = borderWeight;

        for (let i = 0; i < this.numCols; i++) {
            this.map[i] = new Array(this.numRows);
        }

    }

    get(col, row) {
        return this.map[col][row];
    }

    getOnCount() {
        let ret = 0;
        for (let i = 0; i < this.numCols; i++) {
            for (let j = 0; j < this.numRows; j++) {
                if (this.map[i][j].state) {
                    ret++;
                }
            }
        }
        return ret;
    }

    getPutInfo(col, row) {
        const [centerX, centerY] = this.getCenterPos();
        return [centerX + (col * this.buttonSize),
                centerY + ((this.numRows - row - 1) * this.buttonSize),
                this.buttonSize - this.borderWeight];

    }
    putArrowButton(col, row, direction) {
        const [px, py, actualSize] = this.getPutInfo(col, row);

        let btn = new ArrowButton(this, px, py, col, row, actualSize, actualSize, direction);

        this.map[col][row] = btn;
        return btn;
    }

    putNormalButton(col, row, canToggle=true) {
        const [px, py, actualSize] = this.getPutInfo(col, row);

        let btn = new Button(this, px, py, col, row, actualSize, actualSize);
        btn.canToggle = canToggle;

        this.map[col][row] = btn;
        return btn;
    }
    checkRange(x, y) {
        if (x >= 0 && x <= this.numCols - 1) {
            if (y >= 0 && y <= this.numRows - 1) {
                return true;
            }
        }

        return false;
    }

    toggle(x, y) {
        this.map[x][y].onMacro();
    }
    getCenterPos() {
        let centerX = (width * 0.5) - ((this.numCols - 1) * this.buttonSize) * 0.5;
        let centerY = (height * 0.5) - ((this.numRows - 1) * this.buttonSize) * 0.5;
        return [centerX, centerY];
    }
    
    draw() {
        for (let i = 0; i < this.numCols; i++) {
            for (let j = 0; j < this.numRows; j++) {
                this.map[i][j].draw();
            }
        }
    }

    onMouseClicked() {
        for (let i = 0; i < this.numCols; i++) {
            for (let j = 0; j < this.numRows; j++) {
                this.map[i][j].onClicked();
            }
        }
    }

    onMouseReleased() {
        for (let i = 0; i < this.numCols; i++) {
            for (let j = 0; j < this.numRows; j++) {
                this.map[i][j].onReleased();
            }
        }
    }

    updateLayout() {
        const [centerX, centerY] = this.getCenterPos();

        for (let row = 0; row < this.numRows; row++) {
            for (let col = 0; col < this.numCols; col++) {
                let px = centerX + (col * this.buttonSize);
                let py = centerY + ((this.numRows - row - 1) * this.buttonSize);
                
                let btn = this.map[col][row];
                btn.x = px;
                btn.y = py;
            }
        }
    }
}